/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho.de.programacao;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

/**
 *
 * @author Leonardo Adorno e Lucas pellegrini
 * 
 * Classe com os métodos onde adicionaremos 
 * os pontos do atirador
 */
public class FXMLAdicionarPontos4Controller implements Initializable {

        
    @FXML private TextField adicionarAtak4;
    @FXML private TextField adicionarVida4;
    @FXML private Label vidaAtirador;
    @FXML private Label ataqueAtirador;
    @FXML private Text mensagem;
    /**
     * Objeto Atira
     */
    Atirador Atira = new Atirador();
    
    private int vidaNova;
    private int ataqueNovo;
   /**
     * Método para voltar para a tela de configuração
     * @param event 
     */ 
   @FXML 
   void voltar(ActionEvent event){
        TrabalhoDeProgramacao.voltar("FXMLTelaConfig4.fxml");
    }
    /**
     * Método para adicionar os pontos, entretanto, só aceita se a soma 
     * dos valores for menor ou igual à 10
     * @param event 
     */
        @FXML
        void add4(ActionEvent event) {
                if((Integer.parseInt(adicionarVida4.getText())+Integer.parseInt(adicionarAtak4.getText()) >10)){
                    mensagem.setText("Digite dois valores, na qual a soma seja menor que 10!");
                } 
                else{
                    vidaNova = Integer.parseInt(adicionarVida4.getText()) + BD.getInstance().getAtiradorAliado().getVida();
                    BD.getInstance().getAtiradorAliado().setVida(vidaNova);
                    ataqueNovo = Integer.parseInt(adicionarAtak4.getText()) + BD.getInstance().getAtiradorAliado().getAtaque();
                    BD.getInstance().getAtiradorAliado().setAtaque(ataqueNovo);
                    vidaAtirador.setText(Integer.toString(BD.getInstance().getAtiradorAliado().getVida()));
                    ataqueAtirador.setText(Integer.toString(BD.getInstance().getAtiradorAliado().getAtaque()));
               }
        }
   
   /**
    * 
    * @param url
    * @param rb 
    */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        vidaAtirador.setText(Integer.toString(BD.getInstance().getAtiradorAliado().getVida()));
        ataqueAtirador.setText(Integer.toString(BD.getInstance().getAtiradorAliado().getAtaque()));
    }    
    
}
