/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho.de.programacao;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.text.Text;

/**
 *
 * @author Leonardo Adorno e Lucas pellegrini
 * 
 * Classe com os métodos para a troca de tela onde adicionaremos 
 * os pontos do lanceiro
 */
public class FXMLTelaConfigr3Controller implements Initializable {
    /**
     * Declaração das variáveis voltar, vidaLanceiro e ataqueLanceiro
     */
    @FXML private Button voltar;
    @FXML private Text vidaCavaleiro;
    @FXML private Text ataqueCavaleiro;

    /**
     * Método para voltar para a tela de configuração
     * @param event 
     */
    @FXML
    private void voltar(ActionEvent event) {
                TrabalhoDeProgramacao.voltar("FXMLDocument.fxml");
    }
    /**
     * Método para a tela de adicionar pontos
     * @param event 
     */
    @FXML
    private void add3(ActionEvent event) {
        TrabalhoDeProgramacao.trocaTela("FXMLAdicionarPontos3.fxml");
    }
    /**
     * 
     * @param url
     * @param rb 
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        vidaCavaleiro.setText(Integer.toString(BD.getInstance().getCavaleiroAliado().getVida()));
        ataqueCavaleiro.setText(Integer.toString(BD.getInstance().getCavaleiroAliado().getAtaque()));
    }  
}
