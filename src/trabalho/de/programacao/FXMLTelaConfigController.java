/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho.de.programacao;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.text.Text;

/**
 *
 * @author Leonardo Adorno e Lucas pellegrini
 * 
 * Classe com os métodos para a troca de tela onde adicionaremos 
 * os pontos do lanceiro
 */
public class FXMLTelaConfigController implements Initializable {
    /**
     * Declaração das variáveis voltar, vidaLanceiro e ataqueLanceiro
     */
    @FXML private Button voltar;
    @FXML private Text vidaLanceiro;
    @FXML private Text ataqueLanceiro;
    /**
     * Método para voltar para a tela de configuração
     * @param event 
     */
    @FXML private void voltar(ActionEvent event){
        TrabalhoDeProgramacao.voltar("FXMLDocument.fxml");
    }
    
    
    /**
     * Método para a tela de adicionar pontos
     * @param event 
     */
    @FXML private void add(ActionEvent event){
        TrabalhoDeProgramacao.trocaTela("FXMLAdicionaPontos.fxml");
    }
    /**
     * 
     * @param url
     * @param rb 
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        vidaLanceiro.setText(Integer.toString(BD.getInstance().getLanceiroAliado().getVida()));
        ataqueLanceiro.setText(Integer.toString(BD.getInstance().getLanceiroAliado().getAtaque()));
    }    
    
}
