/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho.de.programacao;

import java.util.ArrayList;
import trabalho.de.programacao.Lanceiros;

/**
 *
 * @author Leonardo Adorno 
 * 
 * Classe com os métodos que organizam a batalha, decidem o time vencedor, fazem
 * os personagens atacarem e tiram a vida de personagens que foram atacados
 */
public class BD {
    
    /**
     * Arraylist dos personagens aliados e inimigos
     */
    private ArrayList lanceiro = new ArrayList<>();
    private ArrayList medico = new ArrayList<>();
    private ArrayList cavaleiro = new ArrayList<>();
    private ArrayList atirador = new ArrayList<>();
    private ArrayList lanceiroInimigoq = new ArrayList<>();
    private ArrayList medicoInimigoq = new ArrayList<>();
    private ArrayList cavaleiroInimigoq = new ArrayList<>();
    private ArrayList atiradorInimigoq = new ArrayList<>();
    private static final BD bdSingleton = new BD();
    private Lanceiros lanceiroAliado = new Lanceiros();
    private Medico medicoAliado = new Medico();
    private Cavaleiro cavaleiroAliado = new Cavaleiro();
    private Atirador atiradorAliado = new Atirador();
    private Lanceiros lanceiroInimigo = new Lanceiros();
    private Medico medicoInimigo = new Medico();
    private Cavaleiro cavaleiroInimigo = new Cavaleiro();
    private Atirador atiradorInimigo = new Atirador();
    
    private int fase;
    
/**
 * Método para retornar a quantidade de lanceiros inimigos
 * @return lanceiroInimigoq
 */
    public ArrayList getLanceiroInimigoq() {
        return lanceiroInimigoq;
    }
/**
 * Método onde a quantidade de lanceiros inimigos é passada como parâmetro
 * @param lanceiroInimigoq
 */
    public void setLanceiroInimigoq(ArrayList lanceiroInimigoq) {
        this.lanceiroInimigoq = lanceiroInimigoq;
    }
/**
 * Método para retornar a quantidade de médicos inimigos
 * @return medicoInimigoq
 */
    public ArrayList getMedicoInimigoq() {
        return medicoInimigoq;
    }
/**
 * Método onde a quantidade de médicos inimigos é passada como parâmetro
 * @param medicoInimigoq
 */
    public void setMedicoInimigoq(ArrayList medicoInimigoq) {
        this.medicoInimigoq = medicoInimigoq;
    }
/**
 * Método para retornar a quantidade de cavaleiros inimigos
 * @return cavaleiroInimigoq
 */
    public ArrayList getCavaleiroInimigoq() {
        return cavaleiroInimigoq;
    }
/**
 * Método onde a quantidade de cavaleiros inimigos é passada como parâmetro
 * @param cavaleiroInimigoq
 */
    public void setCavaleiroInimigoq(ArrayList cavaleiroInimigoq) {
        this.cavaleiroInimigoq = cavaleiroInimigoq;
    }
/**
 * Método para retornar a quantidade de atiradores inimigos
 * @return atiradorInimigoq
 */
    public ArrayList getAtiradorInimigoq() {
        return atiradorInimigoq;
    }
/**
 * Método onde a quantidade de atiradores inimigos é passada como parâmetro
 * @param atiradorInimigoq
 */
    public void setAtiradorInimigoq(ArrayList atiradorInimigoq) {
        this.atiradorInimigoq = atiradorInimigoq;
    }
    
/**
 * Método para retornar lanceiros inimigos
 * @return lanceiroInimigoq
 */
    public Lanceiros getLanceiroInimigo() {
        return lanceiroInimigo;
    }
/**
 * Método onde os lanceiros inimigos são passados como parâmetro
 * @param lanceiroInimigo
 */
    public void setLanceiroInimigo(Lanceiros lanceiroInimigo) {
        this.lanceiroInimigo = lanceiroInimigo;
    }
/**
 * Método para retornar médicos inimigos
 * @return medicoInimigoq
 */
    public Medico getMedicoInimigo() {
        return medicoInimigo;
    }
/**
 * Método onde os médicos inimigos são passados como parâmetro
 * @param medicoInimigo
 */
    public void setMedicoInimigo(Medico medicoInimigo) {
        this.medicoInimigo = medicoInimigo;
    }
/**
 * Método para retornar cavaleiros inimigos
 * @return cavaleiroInimigoq
 */
    public Cavaleiro getCavaleiroInimigo() {
        return cavaleiroInimigo;
    }
/**
 * Método onde os cavaleiros inimigos são passados como parâmetro
 * @param cavaleiroInimigo
 */
    public void setCavaleiroInimigo(Cavaleiro cavaleiroInimigo) {
        this.cavaleiroInimigo = cavaleiroInimigo;
    }
/**
 * Método para retornar atiradores inimigos
 * @return atiradorInimigoq
 */
    public Atirador getAtiradorInimigo() {
        return atiradorInimigo;
    }
/**
 * Método onde os atiradores inimigos são passados como parâmetro
 * @param atiradorInimigo
 */
    public void setAtiradorInimigo(Atirador atiradorInimigo) {
        this.atiradorInimigo = atiradorInimigo;
    }
/**
 * Método para retornar lanceiros aliados
 * @return lanceiroAliado
 */    
    public Lanceiros getLanceiroAliado() {
        return lanceiroAliado;
    }
/**
 * Método onde os lanceiros aliados são passados como parâmetro
 * @param lanceiroAliado
 */
    public void setLanceiroAliado(Lanceiros lanceiroAliado) {
        this.lanceiroAliado = lanceiroAliado;
    }
/**
 * Método para retornar médicos aliados
 * @return medicoAliado
 */
    public Medico getMedicoAliado() {
        return medicoAliado;
    }
/**
 * Método onde os médicos aliados são passados como parâmetro
 * @param medicoAliado
 */
    public void setMedicoAliado(Medico medicoAliado) {
        this.medicoAliado = medicoAliado;
    }
/**
 * Método para retornar cavaleiros aliados
 * @return cavaleiroAliado
 */
    public Cavaleiro getCavaleiroAliado() {
        return cavaleiroAliado;
    }
/**
 * Método onde os cavaleiros aliados são passados como parâmetro
 * @param cavaleiroAliado
 */
    public void setCavaleiroAliado(Cavaleiro cavaleiroAliado) {
        this.cavaleiroAliado = cavaleiroAliado;
    }
/**
 * Método para retornar atiradores aliados
 * @return atiradorAliado
 */
    public Atirador getAtiradorAliado() {
        return atiradorAliado;
    }
/**
 * Método onde os atiradores aliados são passados como parâmetro
 * @param  atiradorAliado
 */
    public void setAtiradorAliado(Atirador atiradorAliado) {
        this.atiradorAliado = atiradorAliado;
    }
/**
 * Método para retornar a fase
 * @return fase
 */    
    public int getFase() {
        return fase;
    }
/**
 * Método onde a fase é passada como parâmetro
 * @param fase
 */
    public void setFase(int fase) {
        this.fase = fase;
    }
/**
 * @return bdSingleton
 */       
    public static BD getInstance(){
        return bdSingleton;
    }
/**
 * Método para criar um novo lanceiro
 * @param qtdLanceiro 
 */    
    public void criarLanceiro(int qtdLanceiro){
        for(int i=0;i<qtdLanceiro;i++){
            lanceiro.add(new Lanceiros());
        }
        BD.getInstance().getLanceiroAliado().setQuantidade(qtdLanceiro);
    }
/**
* Métodopara criar um novo médico
* @param qtdMedico 
*/    
    public void criarMedico(int qtdMedico){
        for(int i=0;i<qtdMedico;i++){
            medico.add(new Medico());
        }
        BD.getInstance().getLanceiroAliado().setQuantidade(qtdMedico);
    }
/**
* Método para criar um novo cavaleiro
* @param qtdCavaleiro 
*/    
    public void criarCavaleiro(int qtdCavaleiro){
        for(int i=0;i<qtdCavaleiro;i++){
            cavaleiro.add(new Cavaleiro());
        }
        BD.getInstance().getLanceiroAliado().setQuantidade(qtdCavaleiro);
    }
/**
* Método para criar um novo atirador
* @param qtdAtirador 
*/    
    public void criarAtirador(int qtdAtirador){
        for(int i=0;i<qtdAtirador;i++){
            atirador.add(new Atirador());
        }
        BD.getInstance().getLanceiroAliado().setQuantidade(qtdAtirador);
    }
/**
* Método para criar um novo lanceiro inimigo
* @param qtdLanceiro 
*/    
    public void criarLanceiroInimigo(int qtdLanceiro){
        for(int i=0;i<qtdLanceiro;i++){
            lanceiroInimigoq.add(new Lanceiros());
        }
        BD.getInstance().getLanceiroInimigo().setQuantidade(qtdLanceiro);
    }
/**
* Método para criar um novo médico inimigo
* @param qtdMedico 
*/    
    public void criarMedicoInimigo(int qtdMedico){
        for(int i=0;i<qtdMedico;i++){
            medicoInimigoq.add(new Medico());
        }
        BD.getInstance().getMedicoInimigo().setQuantidade(qtdMedico);
    }
/**
* Método para criar um novo cavaleiro inimigo
* @param qtdCavaleiro 
*/    
    public void criarCavaleiroInimigo(int qtdCavaleiro){
        for(int i=0;i<qtdCavaleiro;i++){
            cavaleiroInimigoq.add(new Cavaleiro());
        }
        BD.getInstance().getCavaleiroInimigo().setQuantidade(qtdCavaleiro);
    }
/**
* Método para criar um novo atirador inimigo
* @param qtdAtirador 
*/    
    public void criarAtiradorInimigo(int qtdAtirador){
        for(int i=0;i<qtdAtirador;i++){
            atiradorInimigoq.add(new Atirador());
        }
        BD.getInstance().getAtiradorInimigo().setQuantidade(qtdAtirador);
    }
/**
 * Método para retornar um lanceiro
 * @return lanceiro
 */
    public ArrayList getLanceiro() {
        return lanceiro;
    }
/**
 * Método que retorna o lanceiro como parâmetro
 * @param lanceiro 
 */
    public void setLanceiro(ArrayList lanceiro) {
        this.lanceiro = lanceiro;
    }
/**
 * Método para retornar um medico
 * @return medico
 */
    public ArrayList getMedico() {
        return medico;
    }
/**
 * Método que retorna o médico como parâmetro
 * @param medico
 */
    public void setMedico(ArrayList medico) {
        this.medico = medico;
    }
/**
 * Método para retornar um cavaleiro
 * @return cavaleiro
 */
    public ArrayList getCavaleiro() {
        return cavaleiro;
    }
/**
 * Método que retorna o cavaleiro como parâmetro
 * @param cavaleiro
 */
    public void setCavaleiro(ArrayList cavaleiro) {
        this.cavaleiro = cavaleiro;
    }
/**
 * Método para retornar um atirador
 * @return atirador
 */
    public ArrayList getAtirador() {
        return atirador;
    }
/**
 * Método que retorna o atirador como parâmetro
 * @param atirador
 */
    public void setAtirador(ArrayList atirador) {
        this.atirador = atirador;
    }
    
/**
       * Método para lançar o atauqe especial do lanceiro e
       * matar 2 lanceiros inimigos
       */    
    public void ataqueEspecialLanceiro(){
        lanceiroInimigoq.remove(1);
        lanceiroInimigoq.remove(2);
    }
/**
       * Método para lançar o atauqe especial do médico e
       * matar 3 lanceiros inimigos
       */    
    public void ataqueEspecialMedico(){
        lanceiroInimigoq.remove(1);
        lanceiroInimigoq.remove(2);
        lanceiroInimigoq.remove(3);
    }
/**
       * Método para lançar o atauqe especial do cavaleiro e
       * matar 3 lanceiros inimigos
       */    
    public void ataqueEspecialCavaleiro(){
        lanceiroInimigoq.remove(1);
        lanceiroInimigoq.remove(2);
        lanceiroInimigoq.remove(3);
        lanceiroInimigoq.remove(4);
    }
/**
       * Método para lançar o atauqe especial do atirador e
       * matar 5 lanceiros inimigos
       */    
    public void ataqueEspecialAtirador(){
        lanceiroInimigoq.remove(1);
        lanceiroInimigoq.remove(2);
        lanceiroInimigoq.remove(3);
        lanceiroInimigoq.remove(4);
        lanceiroInimigoq.remove(5);
    }
/**
     * Método para remover o inimigo do ArrayList
     * @param tipo - String
     */    
    public void mataAlguem(String tipo){
        if(tipo.equals("lanceiro")){
             this.lanceiro.remove(lanceiro.size()-1);
            
        }
         
        if(tipo.equals("medico")){
            this.medico.remove(medico.size()-1);
             
        }
        
        if(tipo.equals("cavaleiro")){
             
                this.cavaleiro.remove(cavaleiro.size()-1);
            }
        
        
        if(tipo.equals("atirador")){
            
                this.atirador.remove(atirador.size()-1);
            
        }
        
        if(tipo.equals("lanceiroInimigo")){
             this.lanceiroInimigoq.remove(lanceiroInimigoq.size()-1);
            
        }
         
        if(tipo.equals("medicoInimigo")){
            this.medicoInimigoq.remove(medicoInimigoq.size()-1);
             
        }
        
        if(tipo.equals("cavaleiroInimigo")){
             
                this.cavaleiroInimigoq.remove(cavaleiroInimigoq.size()-1);
            }
        
        
        if(tipo.equals("atiradorInimigo")){
            
                this.atiradorInimigoq.remove(atiradorInimigoq.size()-1);
            
        }
    }
/**
     * Método para verificar se o aliado e o inimigo, perdeream ou ganharam
     * @param tipo String
     * @return boolean
     */ 
    public boolean vitoriaDerrota(){
        if(lanceiro.size() > 0 && medico.size() > 0 && cavaleiro.size() > 0 && atirador.size() > 0){
            return true;
        }
        else if(lanceiroInimigoq.size() > 0 && medicoInimigoq.size() > 0 && cavaleiroInimigoq.size() > 0 && atiradorInimigoq.size() > 0){
           return true; 
        }
        else{
            return false;
        }
    }
/**
     * Método para verificar se existem inimigos no array
     * @param tipo String
     * @return boolean
     */    
    public boolean inimigosRestantes(){
        return lanceiroInimigoq.size() > 0 || medicoInimigoq.size() > 0 || cavaleiroInimigoq.size() > 0 || atiradorInimigoq.size() > 0;
    }
/**
     * Método para verificar se existem aliados no array
     * @param tipo String
     * @return boolean
     */   
    public boolean aliadosRestantes(){
        return lanceiro.size() > 0 || medico.size() >0 || cavaleiro.size() > 0 || atirador.size() > 0;
    }
/**
     * Método para verificar se existem tropas no array
     * @param tipo String
     * @return boolean
     */ 
    public boolean tropaRestante(String tipo){
        if(tipo.equals("lanceiro")){
            if(lanceiro.size() > 0){
                return true;
            }
        }
        
        else if(tipo.equals("medico")){
            if(medico.size() > 0){
                return true;
            }
        }
        
        else if(tipo.equals("cavaleiro")){
            if(cavaleiro.size() > 0){
                return true;
            }
        }
        
        else if(tipo.equals("atirador")){
            if(atirador.size() > 0){
                return true;
            }
        }
                
        else if(tipo.equals("lanceiroInimigo")){
            if(lanceiroInimigoq.size() > 0){
                return true;
            }
        }
        
        else if(tipo.equals("medicoInimigo")){
            if(medicoInimigoq.size() > 0){
                return true;
            }
        }
        
        else if(tipo.equals("cavaleiroInimigo")){
            if(cavaleiroInimigoq.size() > 0){
                return true;
            }
        }
        
        else if(tipo.equals("atiradorInimigo")){
            if(atiradorInimigoq.size() > 0){
                return true;
            }
        }
       
        return false;
    }
/**
     * Método para verificar quantos personagens ainda estão vivos
     * @param tipo String
     * @return int
     */
    public int quantidadeRestante(String tipo){
        if(tipo.equals("lanceiroInimigo")){
            if(lanceiroInimigoq.size() > 0){
                return lanceiroInimigoq.size();
            }
        }
        
        if(tipo.equals("medicoInimigo")){
            if(medicoInimigoq.size() > 0){
                return medicoInimigoq.size();
            }
        }
        
        if(tipo.equals("cavaleiroInimigo")){
            if(cavaleiroInimigoq.size() > 0){
                return cavaleiroInimigoq.size();
            }
        }
        
        if(tipo.equals("atiradorInimigo")){
            if(atiradorInimigoq.size() > 0){
                return atiradorInimigoq.size();
            }
        }
        
        if(tipo.equals("lanceiro")){
            if(lanceiro.size() > 0){
                return lanceiro.size();
            }
        }
        
        if(tipo.equals("medico")){
            if(medico.size() > 0){
                return medico.size();
            }
        }
        
        if(tipo.equals("cavaleiro")){
            if(cavaleiro.size() > 0){
                return cavaleiro.size();
            }
        }
        
        if(tipo.equals("atirador")){
            if(atirador.size() > 0){
                return atirador.size();
            }
        }
        return 0;
    }
}
