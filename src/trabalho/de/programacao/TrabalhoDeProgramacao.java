/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho.de.programacao;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Leonardo Adorno e Lucas pellegrini
 * 
 * Classe com os métodos responsáveis pela troca de tela
 */
public class TrabalhoDeProgramacao extends Application {
    private static Stage stage;
    /**
    * 
    * @return stage
    */
    public static Stage getStage() {
        return stage;
    }
    
    
       /**
        * Método para a configuração da troca de tela
        * @param tela - String
        */
       public static void trocaTelaConfig(String tela){
        Parent root = null;
        try{
            root = FXMLLoader.load(TrabalhoDeProgramacao.class.getResource(tela));
        }catch(Exception e){
            System.out.println("Verificar arquivo FXML");
        }
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }
    
    
    /**
     * Método para a troca de tela
     * @param tela - String
     */
    public static void trocaTela(String tela){
         FXMLLoader loader = new FXMLLoader(TrabalhoDeProgramacao.class.getResource(tela));
       
       
        try {
             Parent root = loader.load();
            //root = FXMLLoader.load(TrabalhoDeProgramacao.class.getResource(tela));
           
             Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(TrabalhoDeProgramacao.class.getName()).log(Level.SEVERE, null, ex);
        }
       
       
    }
    /**
     * Método para a troca de tela, mais precisamente, para voltar para a
     * tela anterior
     * @param tela - String
     */
    public static void voltar(String tela){
        Parent root = null;
        try{
            root = FXMLLoader.load(TrabalhoDeProgramacao.class.getResource(tela));
        }catch(Exception e){
            System.out.println("Verificar arquivo FXML");
        }
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }


    /**
    * 
    * @param stage 
    * @throws Exception 
    */
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
        TrabalhoDeProgramacao.stage = stage;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
