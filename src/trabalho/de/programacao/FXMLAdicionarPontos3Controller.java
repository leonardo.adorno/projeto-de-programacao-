/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho.de.programacao;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

/**
 *
 * @author Leonardo Adorno e Lucas pellegrini
 * 
 * Classe com os métodos onde adicionaremos 
 * os pontos do cavaleiro
 */
public class FXMLAdicionarPontos3Controller implements Initializable {
    
    @FXML private TextField adicionarVida3;
    @FXML private TextField adicionarAtak3;
    @FXML private Label vidaCavaleiro;
    @FXML private Label ataqueCavaleiro;
    @FXML private Text mensagem;
/**
     * Objeto Cavalo
     */  
    Cavaleiro Cavalo = new Cavaleiro();
    
    private int vidaNova;
    private int ataqueNovo;
    /**
     * Método para voltar para a tela de configuração
     * @param event 
     */
    @FXML private void voltar(ActionEvent event){
        TrabalhoDeProgramacao.voltar("FXMLTelaConfigr3.fxml");
    }
    /**
     * Método para adicionar os pontos, entretanto, só aceita se a soma 
     * dos valores for menor ou igual à 10
     * @param event 
     */
        @FXML
        void add3(ActionEvent event) {
               if((Integer.parseInt(adicionarVida3.getText())+Integer.parseInt(adicionarAtak3.getText()) >10)){
                    mensagem.setText("Digite dois valores, na qual a soma seja menor que 10!");
               } 
               else{
                    vidaNova = Integer.parseInt(adicionarVida3.getText()) + BD.getInstance().getCavaleiroAliado().getVida();
                    BD.getInstance().getCavaleiroAliado().setVida(vidaNova);
                    ataqueNovo = Integer.parseInt(adicionarAtak3.getText()) + BD.getInstance().getCavaleiroAliado().getAtaque();
                    BD.getInstance().getCavaleiroAliado().setAtaque(ataqueNovo);
                    vidaCavaleiro.setText(Integer.toString(BD.getInstance().getCavaleiroAliado().getVida()));
                    ataqueCavaleiro.setText(Integer.toString(BD.getInstance().getCavaleiroAliado().getAtaque()));
               }
        }
    
   
    

    /**
    * 
    * @param url
    * @param rb 
    */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        vidaCavaleiro.setText(Integer.toString(BD.getInstance().getCavaleiroAliado().getVida()));
        ataqueCavaleiro.setText(Integer.toString(BD.getInstance().getCavaleiroAliado().getAtaque()));
    }    
    
}
