/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho.de.programacao;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author Leonardo Adorno e Lucas Pellegrini
 * 
 * Classe com todos os métodos para controlar os botôes FXML da tela inicial do jogo
 */
public class FXMLDocumentController implements Initializable {
    /**
     * Declaração das variáveis batalhar, criarTropas1,criarTropas2,criarTropas3,
     * criarTropas4, configurar, configurar2, configurar3, configurar4, qtdLanceiro,
     * qtdMedico, qtdcavaleiro, qtdAtirador
     */
    @FXML
    private Label batalhar;
    
    @FXML private Label criarTropa1;
    
            
    @FXML private Label criarTropa2;
    
    @FXML private Label criarTropa3;
    
    @FXML private Label criarTropa4;
    
    @FXML private Label configurar;
    
    @FXML private Label configurar2;
    
    @FXML private Label configurar3;
    
    @FXML private Label configurar4;
    
    @FXML private TextField qtdLanceiro;
    
    @FXML private TextField qtdMedico;
    
    @FXML private TextField qtdCavaleiro;
    
    @FXML private TextField qtdAtirador;
    
    

    
    
     /**
     * Método que contém o botão para a primeira fase do jogo
     * @param event 
     */
    @FXML
    private void Fase1(ActionEvent event) {
       BD.getInstance().setFase(1);
       TrabalhoDeProgramacao.trocaTela("FXMLBatalha.fxml");
    }
    /**
     * Método que contém o botão para a segunda fase do jogo
     * @param event 
     */
     @FXML
    private void Fase2(ActionEvent event) {
       BD.getInstance().setFase(2);
       TrabalhoDeProgramacao.trocaTela("FXMLBatalha.fxml");
    }
    /**
     * Método que contém o botão para a terceira fase do jogo
     * @param event 
     */
     @FXML
    private void Fase3(ActionEvent event) {
       BD.getInstance().setFase(3);
       TrabalhoDeProgramacao.trocaTela("FXMLBatalha.fxml");
       
    }
    /**
     * Método que contém o botão para a quarta fase do jogo
     * @param event 
     */
     @FXML
    private void Fase4(ActionEvent event) {
        BD.getInstance().setFase(4);
       TrabalhoDeProgramacao.trocaTela("FXMLBatalha.fxml");
       
    }
    /**
     * Método para a criação da tropa de lanceiros
     * @param event 
     */
    @FXML private void criarTropa1(ActionEvent event){
        criarTropa1.setText("Criando tropas!");
        criarTropas("lanceiro", Integer.parseInt(qtdLanceiro.getText()));
    }
    /**
     * Método para a criação da tropa de médicos
     * @param event 
     */
    @FXML private void criarTropa2(ActionEvent event){
        criarTropa2.setText("Criando tropas!");
        criarTropas("medico", Integer.parseInt(qtdMedico.getText()));
    }
    /**
     * Método para a criação da tropa de cavaleiros
     * @param event 
     */
    @FXML private void criarTropa3(ActionEvent event){
        criarTropa3.setText("Criando tropas!");
        criarTropas("cavaleiro", Integer.parseInt(qtdCavaleiro.getText()));
    }
    /**
     * Método para a criação da tropa de atiradores
     * @param event 
     */
    @FXML private void criarTropa4(ActionEvent event){
        criarTropa4.setText("Criando tropas!");
        criarTropas("atirador", Integer.parseInt(qtdAtirador.getText()));
    }
    /**
     * Método com a troca de tela para a configuração dos laceiros
     * @param event 
     */
    @FXML private void configTropa1(ActionEvent event){
        TrabalhoDeProgramacao.trocaTela("FXMLTelaConfig.fxml");
    }
    /**
     * Método com a troca de tela para a configuração dos médicos
     * @param event 
     */
    @FXML private void configTropa2(ActionEvent event){
        TrabalhoDeProgramacao.trocaTela("FXMLTelaConfig2.fxml");
    }
    /**
     * Método com a troca de tela para a configuração dos cavaleiros
     * @param event 
     */
    @FXML private void configTropa3(ActionEvent event){
        TrabalhoDeProgramacao.trocaTela("FXMLTelaConfigr3.fxml");
    }
    /**
     * Método com a troca de tela para a configuração dos atiradores
     * @param event 
     */
    @FXML private void configTropa4(ActionEvent event){
        TrabalhoDeProgramacao.trocaTela("FXMLTelaConfig4.fxml");
    }
    
    
    /**
     * 
     * @param url
     * @param rb 
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    /**
     * Método para a criação de personagens de acordo com a quantidade escolhida
     * pelo usuário
     * @param tropa - String
     * @param qtd - int
     */
    private void criarTropas(String tropa, int qtd){
        
            if(tropa.equals("lanceiro")){
                BD.getInstance().criarLanceiro(qtd);
            }
            else if(tropa.equals("medico")){
                BD.getInstance().criarMedico(qtd);
            }
            else if(tropa.equals("cavaleiro")){
                BD.getInstance().criarCavaleiro(qtd);
            }
            else if(tropa.equals("atirador")){
                BD.getInstance().criarAtirador(qtd);
            }
            
        }
            
    }
    
    

