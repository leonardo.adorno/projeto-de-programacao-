/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho.de.programacao;

/**
 *
 * @author Leonardo Adorno e Lucas Pellegrini
 * 
 * Classe com todos os métodos para a adição de vida e ataque dos personagens
 */
public class Personagem {
    private int ataque;
    private int vida;
    private String tipo;
    private int vidaMax;
    
     /**
     * Método onde a vida, o ataque e o tipo do personagem são passados 
     * como parâmetro
     * @param tipo - String
     * @param vida - int
     * @param ataque - int
     */
    public Personagem(String tipo, int vida, int ataque){
        this.tipo = tipo;
        this.vida = vida;
        this.ataque = ataque;
    }
    /**
     * Método que retorna o máximo de vida do personagem
     * @return vidaMax - int
     */
    public int getVidaMax() {
        return vidaMax;
    }
    /**
     * Método onde o máximo de vida do personagem é passado como parâmetro
     * @param vidaMax - int
     */
    public void setVidaMax(int vidaMax) {
        this.vidaMax = vidaMax;
    }
    /**
     * Método que retorna o valor de ataque do personagem
     * @return ataque - int
     */
    public int getAtaque() {
        return ataque;
    }
    /**
     * Método que passa o valor de ataque do personagem como parâmetro
     * @param ataque - int
     */
    public void setAtaque(int ataque) {
        this.ataque = ataque;
    }
    /**
     * Método que retorna a vida do personagem
     * @return vida - int
     */
    public int getVida() {
        return vida;
    }
    /**
     * Método que passa a vida do personagem como parâmetro
     * @param vida - int
     */
    public void setVida(int vida) {
        this.vida = vida;
    }
    /**
     * Método que retorna o tipo do personagem
     * @return tipo - String
     */
    public String getTipo() {
        return tipo;
    }
    /**
     * Método que passa o tipo do personagem como parâmetro
     * @param tipo - String
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    
    
}
