/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho.de.programacao;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

/**
 *
 * @author Leonardo Adorno e Lucas pellegrini
 * 
 * Classe com os métodos onde adicionaremos 
 * os pontos do lanceiro
 */
public class FXMLAdicionaPontosController implements Initializable {

    
     @FXML private TextField adicionarVida1;
     @FXML private TextField adicionarAtak;
     @FXML private Text vidaLanceiro;
     @FXML private Text ataqueLanceiro;
     @FXML private Text mensagem;
    /**
      * Objeto Lancelote
      */ 
     Lanceiros Lancelote = new Lanceiros();
     private int vidaNova;
     private int ataqueNovo;
     
    /**
     * Método para voltar para a tela de configuração
     * @param event 
     */
    @FXML private void voltar(ActionEvent event){
        TrabalhoDeProgramacao.voltar("FXMLTelaConfig.fxml");
    }
    
    
   
        /**
     * Método para adicionar os pontos, entretanto, só aceita se a soma 
     * dos valores for menor ou igual à 10
     * @param event 
     */
        @FXML
        void add(ActionEvent event) {
               if((Integer.parseInt(adicionarVida1.getText())+Integer.parseInt(adicionarAtak.getText()) >10)){
                    mensagem.setText("Digite dois valores, na qual a soma seja menor que 10!");
                } 
               else{
                    vidaNova = Integer.parseInt(adicionarVida1.getText()) + BD.getInstance().getLanceiroAliado().getVida();
                    BD.getInstance().getLanceiroAliado().setVida(vidaNova);
                    ataqueNovo = Integer.parseInt(adicionarAtak.getText()) + BD.getInstance().getLanceiroAliado().getAtaque();
                    BD.getInstance().getLanceiroAliado().setAtaque(ataqueNovo);
                    vidaLanceiro.setText(Integer.toString(BD.getInstance().getLanceiroAliado().getVida()));
                    ataqueLanceiro.setText(Integer.toString(BD.getInstance().getLanceiroAliado().getAtaque()));
                    
               }
        }
    
    
/**
    * 
    * @param url
    * @param rb 
    */

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        vidaLanceiro.setText(Integer.toString(BD.getInstance().getLanceiroAliado().getVida()));
        ataqueLanceiro.setText(Integer.toString(BD.getInstance().getLanceiroAliado().getAtaque()));
    }    

    
    
}
