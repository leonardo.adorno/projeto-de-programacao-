/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho.de.programacao;

/**
/**
 *
 * @author Leonardo Adorno e Lucas pellegrini
 * 
 * Classe para objetos do tipo médico, onde serão contidos métodos, quantidade
 * de vida e ataque
 */
public class Medico {
    int vida=150;
    int ataque=15;
    int quantidade;
    /**
    * Método para o retorno da quantidade de médicos no jogo
    * @return int - quantidade
    */
    public int getQuantidade() {
        return quantidade;
    }
    /**
    * Método onde a quantidade de médicos é passada como parâmetro
    * @param quantidade int
    */
    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
    /**
    * Método para o retorno da vida do personagem 
    * @return int - vida
    */
    public int getVida() {
        return vida;
    }
    /**
    * Método onde a quantidade de vida do personagem é passada como parâmetro
    * @param vida 
    */
    public void setVida(int vida) {
        this.vida = vida;
    }
    /**
    * Método para o retorno do valor de ataque do personagem 
    * @return int - ataque
    */
    public int getAtaque() {
        return ataque;
    }
    /**
    * Método onde a quantidade de ataque do personagem é passada como parâmetro
    * @param ataque
    */
    public void setAtaque(int ataque) {
        this.ataque = ataque;
    }
    
    
}
