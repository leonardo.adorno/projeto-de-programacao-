Lanceiros Negros:
Características: 
  - Possuem um cavalo;
  - Possuem uma faca;
  - Possuem uma lança;
  - Possuem 100 de vida;
Poder especial: 
  - Com sua lança poderosa, ele consegue eliminar mais 2 inimigos.



Cavaleiros:
Características:
   - Possuem espadas;
   - Possuem uma revolver;
   - Possuem um cavalo;
   - Possuem 100 de vida;
Poder especial:
    - Com sua granada de fragmentação, ele consegue eliminar mais 3 inimigos.


Atiradores:
Características:
    - Possuem um mosquete;
    - Possuem uma faca;
    - Possuem bandagens;
    - Possuem 150 de vida
Poder especial:
    - Com sua espada afiada, ele consegue matar mais 4 inimigos.


 Médico:
 Características:
    - Possuem uma faca;
    - Possuem um escudo;
    - Possuem kits medicos;
    - Possuem 150 de vida;
Poder especial:
    - Com sua sniper pesada, ele consegue matar mais 5 inimigos.
