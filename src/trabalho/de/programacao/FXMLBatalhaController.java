/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho.de.programacao;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;

/**
 *
 * @author Leonardo Adorno e Lucas pellegrini
 * 
 * Classe com os métodos que organizam a batalha, decidem o time vencedor, fazem
 * os personagens atacarem e tiram a vida de personagens que foram atacados
 */
public class FXMLBatalhaController implements Initializable {

    @FXML
    private Label quantidadeLanceiroAliado;
    
    @FXML
    private Label quantidadeMedicoAliado;
    
    @FXML
    private Label quantidadeCavaleiroAliado;
    
    @FXML
    private Label quantidadeAtiradorAliado;
    
    @FXML
    private Label quantidadeLanceiroInimigo;
    
    @FXML
    private Label quantidadeMedicoInimigo;
    
    @FXML
    private Label quantidadeCavaleiroInimigo;
    
    @FXML
    private Label quantidadeAtiradorInimigo;
    
    @FXML
    private Label vidaLanceiroAliado;
    
    @FXML
    private Label vidaMedicoAliado;
    
    @FXML
    private Label vidaCavaleiroAliado;
    
    @FXML
    private Label vidaAtiradorAliado;
    
    @FXML
    private Label vidaLanceiroInimigo;
    
    @FXML
    private Label vidaMedicoInimigo;
    
    @FXML
    private Label vidaCavaleiroInimigo;
    
    @FXML
    private Label vidaAtiradorInimigo;
    
    @FXML
    private Label fase;
    
    @FXML
    private Label vitoria;
    
    @FXML
    private Label ataqueEspecialLanceiro1;
    
    @FXML
    private Label ataqueEspecialMedico2;
    
    @FXML
    private Label ataqueEspecialCavaleiro3;
    
    @FXML
    private Label ataqueEspecialAtirador4;
    /**
     * Método em que o progrma é começado e é decidido o time vencedor
     * @param Event 
     */
    @FXML
    private void Começar(ActionEvent Event){
        while((BD.getInstance().tropaRestante(aliado1.getTipo())||
                BD.getInstance().tropaRestante(aliado2.getTipo())||
                BD.getInstance().tropaRestante(aliado3.getTipo())||
                BD.getInstance().tropaRestante(aliado4.getTipo()))&&
                (BD.getInstance().tropaRestante(inimigo1.getTipo())||
                BD.getInstance().tropaRestante(inimigo2.getTipo())||
                BD.getInstance().tropaRestante(inimigo3.getTipo())||
                BD.getInstance().tropaRestante(inimigo4.getTipo()))){
            atacar();
        }
        
        if(BD.getInstance().tropaRestante(aliado1.getTipo())&&
                BD.getInstance().tropaRestante(aliado2.getTipo())&&
                BD.getInstance().tropaRestante(aliado3.getTipo())&&
                BD.getInstance().tropaRestante(aliado4.getTipo())){
            vitoria.setText("Vitória Royale!");
            
        }
        else{
            vitoria.setText("Derrota!");
        }
    } 
    
    
    /**
     * Objetos dos personagens aliados e inimigos
     */
    Personagem aliado1 = new Personagem("lanceiro", BD.getInstance().getLanceiroAliado().getVida(), BD.getInstance().getLanceiroAliado().getAtaque());
    Personagem aliado2 = new Personagem("medico", BD.getInstance().getMedicoAliado().getVida(), BD.getInstance().getMedicoAliado().getAtaque());
    Personagem aliado3 = new Personagem("cavaleiro", BD.getInstance().getCavaleiroAliado().getVida(), BD.getInstance().getCavaleiroAliado().getAtaque());
    Personagem aliado4 = new Personagem("atirador", BD.getInstance().getAtiradorAliado().getVida(), BD.getInstance().getAtiradorAliado().getAtaque());
    Personagem inimigo1 = new Personagem("lanceiroInimigo", BD.getInstance().getLanceiroInimigo().getVida(), BD.getInstance().getLanceiroInimigo().getAtaque());
    Personagem inimigo2 = new Personagem("medicoInimigo", BD.getInstance().getMedicoInimigo().getVida(), BD.getInstance().getMedicoInimigo().getAtaque());
    Personagem inimigo3 = new Personagem("cavaleiroInimigo", BD.getInstance().getCavaleiroInimigo().getVida(), BD.getInstance().getCavaleiroInimigo().getAtaque());
    Personagem inimigo4 = new Personagem("atiradorInimigo", BD.getInstance().getAtiradorInimigo().getVida(), BD.getInstance().getAtiradorInimigo().getAtaque());
    int especial=0;
    
    
    
    
    /**
     * Método onde acontecem os ataques de um personagem no outro e acontece o especial
     */
    private void atacar() {
       if(especial == 0){
           ataqueEspecialLanceiro();
           ataqueEspecialMedico();
           ataqueEspecialCavaleiro();
           ataqueEspecialAtirador();
       }
       especial=1;
            ataqueHibrido(aliado1, inimigo1);
            ataqueHibrido(aliado2, inimigo2);
            ataqueHibrido(aliado3, inimigo3);
            ataqueHibrido(aliado4, inimigo4);
            ataqueHibrido(inimigo1, aliado1);
        
            ataqueHibrido(inimigo2, aliado2);
            ataqueHibrido(inimigo3, aliado3);
        
            ataqueHibrido(inimigo4, aliado4);
        
        
        quantidadeLanceiroAliado.setText(Integer.toString(BD.getInstance().quantidadeRestante("lanceiro")));
        quantidadeMedicoAliado.setText(Integer.toString(BD.getInstance().quantidadeRestante("medico")));
        quantidadeCavaleiroAliado.setText(Integer.toString(BD.getInstance().quantidadeRestante("cavaleiro")));
        quantidadeAtiradorAliado.setText(Integer.toString(BD.getInstance().quantidadeRestante("atirador")));
        quantidadeLanceiroInimigo.setText(Integer.toString(BD.getInstance().quantidadeRestante("lanceiroInimigo")));
        quantidadeMedicoInimigo.setText(Integer.toString(BD.getInstance().quantidadeRestante("medicoInimigo")));
        quantidadeCavaleiroInimigo.setText(Integer.toString(BD.getInstance().quantidadeRestante("cavaleiroInimigo")));
        quantidadeAtiradorInimigo.setText(Integer.toString(BD.getInstance().quantidadeRestante("atiradorInimigo")));
        

    }
    /**
       * Método onde os personagens aliados e inimigos perdem vida
       * de acordo com os ataques dos oponentes
       * @param personagem1
       * @param personagem2 
       */  
    public void ataqueHibrido(Personagem personagem1, Personagem personagem2){
        if((BD.getInstance().quantidadeRestante(personagem2.getTipo())>0)&&(BD.getInstance().quantidadeRestante(personagem1.getTipo())>0)){
           int ataque1 = personagem2.getVida() - personagem1.getAtaque();
        personagem2.setVida(ataque1);
          atualizaVida(personagem2);
        if(personagem2.getVida()<=0){ 
            BD.getInstance().mataAlguem(personagem2.getTipo());    
            personagem2.setVida(personagem2.getVidaMax());
        }
           
        }else{
            
        }
       
         }
    
    /**
     * Método onde o vê se o Lanceiro tem chance de acertar o ataque especial
     */
    public void ataqueEspecialLanceiro(){
        int rand = (int)(Math.random() * 3);
        if(rand == 2){
            BD.getInstance().ataqueEspecialLanceiro();
            ataqueEspecialLanceiro1.setText("Usou o especial!");
        }
        
    }
    /**
     * Método onde o vê se o Medico tem chance de acertar o ataque especial
     */
    public void ataqueEspecialMedico(){
        int rand = (int)(Math.random() * 3);
        if(rand == 2){
            BD.getInstance().ataqueEspecialMedico();
            ataqueEspecialMedico2.setText("Usou o especial!");
        }
    }
    /**
     * Método onde o vê se o Cavaleiro tem chance de acertar o ataque especial
     */
     public void ataqueEspecialCavaleiro(){
        int rand = (int)(Math.random() * 3);
        if(rand == 2){
            BD.getInstance().ataqueEspecialCavaleiro();
            ataqueEspecialCavaleiro3.setText("Usou o especial!");
        }
    }
    /**
     * Método onde o vê se o Atiradot tem chance de acertar o ataque especial
     */
    public void ataqueEspecialAtirador(){
        int rand = (int)(Math.random() * 3);
        if(rand == 2){
            BD.getInstance().ataqueEspecialAtirador();
            ataqueEspecialAtirador4.setText("Usou o especial!");
        }
    }

    /**
     * Método onde a vida dos personagens é atualizada
     * @param personagem 
     */
    public void atualizaVida(Personagem personagem){
        if(personagem.getTipo().equals("lanceiro")){
            vidaLanceiroAliado.setText(Integer.toString(aliado1.getVida()));
            if(aliado1.getVida() < 0){
                vidaLanceiroAliado.setText("0");   
            }
        }
        if(personagem.getTipo().equals("medico")){
            vidaMedicoAliado.setText(Integer.toString(aliado2.getVida()));
            if(aliado2.getVida() < 0){
                vidaMedicoAliado.setText("0");   
            }
        }
        if(personagem.getTipo().equals("cavaleiro")){
            vidaCavaleiroAliado.setText(Integer.toString(aliado3.getVida()));
            if(aliado3.getVida() < 0){
                vidaCavaleiroAliado.setText("0");   
            }
        }
        if(personagem.getTipo().equals("atirador")){
            vidaAtiradorAliado.setText(Integer.toString(aliado4.getVida()));
            if(aliado4.getVida() < 0){
                vidaAtiradorAliado.setText("0");   
            }
        }
        if(personagem.getTipo().equals("lanceiroInimigo")){
            vidaLanceiroInimigo.setText(Integer.toString(inimigo1.getVida()));
            if(inimigo1.getVida() < 0){
                vidaLanceiroInimigo.setText("0");   
            }
        }
        if(personagem.getTipo().equals("medicoInimigo")){
            vidaMedicoInimigo.setText(Integer.toString(inimigo2.getVida()));
            if(inimigo2.getVida() < 0){
                vidaMedicoInimigo.setText("0");   
            }
        }
        if(personagem.getTipo().equals("cavaleiroInimigo")){
            vidaCavaleiroInimigo.setText(Integer.toString(inimigo3.getVida()));
            if(inimigo3.getVida() < 0){
                vidaCavaleiroInimigo.setText("0");   
            }
        }
        if(personagem.getTipo().equals("atiradorInimigo")){
            vidaAtiradorInimigo.setText(Integer.toString(inimigo4.getVida()));
            if(inimigo4.getVida() < 0){
                vidaAtiradorInimigo.setText("0");   
            }
        }
        
    }
    
   
    
    /**
     * 
     * @param url
     * @param rb 
     */     
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        switch (BD.getInstance().getFase()) {
            case 1:
                fase.setText("Batalha da Lagoa Vermelha");
                BD.getInstance().criarLanceiroInimigo(10);
                BD.getInstance().criarMedicoInimigo(10);
                BD.getInstance().criarCavaleiroInimigo(10);
                BD.getInstance().criarAtiradorInimigo(10);
                break;
            case 2:
                fase.setText("Batalha da Lagoa dos Patos");
                BD.getInstance().criarLanceiroInimigo(20);
                BD.getInstance().criarMedicoInimigo(20);
                BD.getInstance().criarCavaleiroInimigo(20);
                BD.getInstance().criarAtiradorInimigo(20);
                break;
            case 3:
                fase.setText("Batalha dos Farrapos");
                BD.getInstance().criarLanceiroInimigo(30);
                BD.getInstance().criarMedicoInimigo(30);
                BD.getInstance().criarCavaleiroInimigo(30);
                BD.getInstance().criarAtiradorInimigo(30);
                break;
            case 4:
                fase.setText("Batalha Sangrenta");
                BD.getInstance().criarLanceiroInimigo(40);
                BD.getInstance().criarMedicoInimigo(40);
                BD.getInstance().criarCavaleiroInimigo(40);
                BD.getInstance().criarAtiradorInimigo(40);
                break;
            default:
                break;
        }
        quantidadeLanceiroAliado.setText(Integer.toString(BD.getInstance().quantidadeRestante
                    ("lanceiro")));
        quantidadeMedicoAliado.setText(Integer.toString(BD.getInstance().quantidadeRestante
                    ("medico")));
        quantidadeCavaleiroAliado.setText(Integer.toString(BD.getInstance().quantidadeRestante
                    ("cavaleiro")));
        quantidadeAtiradorAliado.setText(Integer.toString(BD.getInstance().quantidadeRestante
                    ("atirador")));
        quantidadeLanceiroInimigo.setText(Integer.toString(BD.getInstance().quantidadeRestante
                    ("lanceiroInimigo")));
        quantidadeMedicoInimigo.setText(Integer.toString(BD.getInstance().quantidadeRestante
                    ("medicoInimigo")));
        quantidadeCavaleiroInimigo.setText(Integer.toString(BD.getInstance().quantidadeRestante
                    ("cavaleiroInimigo")));
        quantidadeAtiradorInimigo.setText(Integer.toString(BD.getInstance().quantidadeRestante
                    ("atiradorInimigo")));
        vidaLanceiroAliado.setText(Integer.toString(aliado1.getVida()));
        vidaMedicoAliado.setText(Integer.toString(aliado2.getVida()));
        vidaCavaleiroAliado.setText(Integer.toString(aliado3.getVida()));
        vidaAtiradorAliado.setText(Integer.toString(aliado4.getVida()));
        vidaLanceiroInimigo.setText(Integer.toString(inimigo1.getVida()));
        vidaMedicoInimigo.setText(Integer.toString(inimigo2.getVida()));
        vidaCavaleiroInimigo.setText(Integer.toString(inimigo3.getVida()));
        vidaAtiradorInimigo.setText(Integer.toString(inimigo4.getVida()));
        
        aliado1.setVidaMax(aliado1.getVida());
        aliado2.setVidaMax(aliado2.getVida());
        aliado3.setVidaMax(aliado3.getVida());
        aliado4.setVidaMax(aliado4.getVida());
        inimigo1.setVidaMax(inimigo1.getVida());
        inimigo2.setVidaMax(inimigo2.getVida());
        inimigo3.setVidaMax(inimigo3.getVida());
        inimigo4.setVidaMax(inimigo4.getVida());
        
        
        
    }    
    
}
