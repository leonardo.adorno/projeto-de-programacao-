/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho.de.programacao;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

/**
 *
 * @author Leonardo Adorno e Lucas pellegrini
 * 
 * Classe com os métodos onde adicionaremos 
 * os pontos do médico
 */
public class FXMLAdicionaPontos2Controller implements Initializable {

    
    @FXML private TextField adicionarVida2;
    @FXML private TextField adicionarAtak2;
    @FXML private Label vidaMedico;
    @FXML private Label ataqueMedico;
    @FXML private Text mensagem;
   /**
     * Objeto medium
     */ 
    Medico medium = new Medico();
    
    private int vidaNova;
    private int ataqueNovo;
    /**
     * Método para voltar para a tela de configuração
     * @param event 
     */ 
    @FXML private void voltar(ActionEvent event){
        TrabalhoDeProgramacao.voltar("FXMLTelaConfig2.fxml");
    }
    /**
     * Método para adicionar os pontos, entretanto, só aceita se a soma 
     * dos valores for menor ou igual à 10
     * @param event 
     */
        @FXML
        void add2(ActionEvent event) {
               if((Integer.parseInt(adicionarVida2.getText())+Integer.parseInt(adicionarAtak2.getText()) >10)){
                    mensagem.setText("Digite dois valores, na qual a soma seja menor que 10!");
               } 
               else{
                    vidaNova = Integer.parseInt(adicionarVida2.getText()) + BD.getInstance().getMedicoAliado().getVida();
                    BD.getInstance().getMedicoAliado().setVida(vidaNova);
                    ataqueNovo = Integer.parseInt(adicionarAtak2.getText()) + BD.getInstance().getMedicoAliado().getAtaque();
                    BD.getInstance().getMedicoAliado().setAtaque(ataqueNovo);
                    vidaMedico.setText(Integer.toString(BD.getInstance().getMedicoAliado().getVida()));
                    ataqueMedico.setText(Integer.toString(BD.getInstance().getMedicoAliado().getAtaque()));
               }
        }
    
    
/**
    * 
    * @param url
    * @param rb 
    */

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        vidaMedico.setText(Integer.toString(BD.getInstance().getMedicoAliado().getVida()));
        ataqueMedico.setText(Integer.toString(BD.getInstance().getMedicoAliado().getAtaque()));
    }    
    
}
